class Division < ApplicationRecord
  has_many :customers, dependent: :destroy
  has_many :districts, dependent: :destroy
end
