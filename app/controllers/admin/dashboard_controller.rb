class Admin::DashboardController < Admin::ApplicationController
  def index
    @customers = Customer.all
    @divisions = Division.all
    @districts = District.all
  end
end
