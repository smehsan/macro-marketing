class Admin::CustomersController < Admin::DashboardController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  
  def index
    @customers = Customer.all.order("Created_at DESC")
  end

  def new
    @customer = Customer.new
  end

  def create
    @customer = Customer.create(customer_params)
    if @customer.save
      flash[:success] = "Customer Create Successfully."
      redirect_to admin_customers_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @customer.update(customer_params)
      flash[:success] = "Customer details update successfully."
      redirect_to admin_customers_path
    else
      render 'edit'
    end
  end

  def destroy
    if @customer.destroy
      flash[:danger] = "Customer deleted successfully"
      redirect_to admin_customers_path
    else
      flash[:danger] = "Something went wrong!"
      redirect_to admin_customers_path
    end
  end
  

  private
  def customer_params
    params.require(:customer).permit(:name, :mobilenumber, :urgentnumber, :presentaddress, :permanentaddress, :division_id, :district_id)
  end

  def set_customer
    @customer = Customer.find(params[:id])
  end
end
