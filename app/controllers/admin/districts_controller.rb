class Admin::DistrictsController < Admin::DashboardController
  before_action :set_district, only: [:show, :edit, :update, :destroy]
  
  def index
    @districts = District.all
  end

  def new
    @district = District.new
  end

  def create
    @district = District.create(district_params)
    if @district.save
      flash[:success] = "District Create Successfully."
      redirect_to admin_districts_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @district.update(district_params)
      flash[:success] = "District details update successfully."
      redirect_to admin_districts_path
    else
      render 'edit'
    end
  end

  def destroy
    if @district.destroy
      flash[:danger] = "District deleted successfully"
      redirect_to admin_districts_path
    else
      flash[:danger] = "Something went wrong!"
      redirect_to admin_districts_path
    end
  end
  

  private
  def district_params
    params.require(:district).permit(:name, :division_id)
  end

  def set_district
    @district = District.find(params[:id])
  end
end
