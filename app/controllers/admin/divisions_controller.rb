class Admin::DivisionsController < Admin::DashboardController
  before_action :set_division, only: [:show, :edit, :update, :destroy]
  
  def index
    @divisions = Division.all
  end

  def new
    @division = Division.new
  end

  def create
    @division = Division.create(division_params)
    if @division.save
      flash[:success] = "Division Create Successfully."
      redirect_to admin_divisions_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @division.update(division_params)
      flash[:success] = "Division details update successfully."
      redirect_to admin_divisions_path
    else
      render 'edit'
    end
  end

  def destroy
    if @division.destroy
      flash[:danger] = "Division deleted successfully"
      redirect_to admin_divisions_path
    else
      flash[:danger] = "Something went wrong!"
      redirect_to admin_divisions_path
    end
  end
  

  private
  def division_params
    params.require(:division).permit(:name)
  end

  def set_division
    @division = Division.find(params[:id])
  end
end
