class AddDivisionToCustomers < ActiveRecord::Migration[6.0]
  def change
    add_reference :customers, :division, null: false, foreign_key: true
  end
end
