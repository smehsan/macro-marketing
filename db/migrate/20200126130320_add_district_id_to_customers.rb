class AddDistrictIdToCustomers < ActiveRecord::Migration[6.0]
  def change
    add_reference :customers, :district, null: false, foreign_key: true
  end
end
