class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :mobilenumber
      t.string :urgentnumber
      t.text :presentaddress
      t.text :permanentaddress

      t.timestamps
    end
  end
end
