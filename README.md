# Macro Marketing
    Macro Marketing(Macrom) is a Mrketing App for data collection of customer which is cloud-enabled, mobile-ready and powered by Ruby on Rails (Version-6).

## Features!
    Add Customer
    Add Division, District and root customer address

## Tech
Dillinger uses a number of open source projects to work properly:

    [HTML5, CSS3] - HTML  CSS3 enhanced for web apps!
    [Twitter Bootstrap] - great UI boilerplate for modern web apps
    [webpack] - webpack is a module bundler.
    [jQuery] - duh
    [Ace Editor] - awesome web-based text editor

## Process
    Admin
      Dashboard
---
    Customer
      Image
      Full Name
      Problem Details
      Contact Details
      Present Address
      Permanent Address
      Urgent Contact
---   
    Address
      Division
      District
      Thana
      Sub-Office
      Post Code
---
    User
      Admin
      User
---
    Route
      /admin/dashboard
      
      /admin/customer/new
      /admin/customer/id/edit
      /admin/customer/id/delete

      /admin/division/new
      /admin/division/id/edit
      /admin/division/id/delete
---
    Controller
      DashboardController
      CustomersController
    Model
      Customer
---
    Customer has one Division
    Division has any District
    District has many Thana
    Thana has many sub-office
    Sub-Office has a Post-Code